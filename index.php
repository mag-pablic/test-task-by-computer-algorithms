<?php

ini_set( 'max_execution_time', 10000 );

//how many load products, max 1340
define( 'PRODUCTS_TOTAL', 1340 );
//max 100
define( 'LIMIT', 100 );

function get_product_details_html($product_id)
{
    $ch = curl_init( 'https://windshiptrading.com/products.php?productId=' . $product_id );

    curl_setopt( $ch, CURLOPT_HTTPHEADER, [
        'stencil-options: {"render_with":"products/quick-view"}'
    ] );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, TRUE );

    $html = curl_exec( $ch );

    curl_close( $ch );

    return $html;
}

function get_product_thumbs($product_details_html)
{
    $matches = [];
    preg_match_all( '/https?[^"]*\.(jpg|jpeg|png|webp)[^"]*(?=")/i', $product_details_html, $matches );
    $images = array_unique( $matches[0] );
    $images = array_values( $images );
    return $images;
}

function get_product_name($product_details_html)
{
    $matches = [];
    preg_match( '/<h1.*>(.*)<\/h1>/', $product_details_html, $matches );
    $name = $matches[1];
    return $name;
}

function get_product_sku($product_details_html)
{
    $matches = [];
    preg_match( '/<dd class="productView-info-value productView-info-value--sku".*>(.*)<\/dd>/', $product_details_html, $matches );
    $sku = $matches[1];
    return $sku;
}

function get_product_description($product_details_html)
{
    $matches = [];
    $product_details_html = preg_replace( "/\r|\n/", "", $product_details_html );
    preg_match( '/<article[^>]*>(.*)<\/article>/', $product_details_html, $matches );
    $payload = $matches[1];
    preg_match_all( '/(?<=>)[^<]*/', $payload, $matches );
    $matches = $matches[0];
    array_shift( $matches );

    $desc = '';
    for ( $i = 0; $i < count( $matches ); $i++ ) {
        $str = html_entity_decode( trim( $matches[$i] ) );

        if ( $str === 'Extra Information' ) {
            break;
        }

        if ( $str !== '' && $desc !== '' ) {
            $desc .= ' ';
        }

        $desc .= $str;
    }

    return $desc;
}

function get_product_brand($product_details_html)
{
    $matches = [];
    preg_match_all( '/<div class="productView-table-header">.*<\/div>[^<>]*[^<]*<div[^>]*>(.*)<\/div>/', $product_details_html, $matches );
    $brand = $matches[1][0];
    return $brand;
}

function fetch_product_data($product_id)
{
    $product_details_html = get_product_details_html( $product_id );
    $thumbs = get_product_thumbs( $product_details_html );
    $name = get_product_name( $product_details_html );
    $sku = get_product_sku( $product_details_html );
    $description = get_product_description( $product_details_html );
    $product_data = compact( 'thumbs', 'name', 'sku', 'description' );
    $brand = get_product_brand( $product_details_html );

    if ( $brand ) {
        $product_data['brand'] = $brand;
    }

    return $product_data;
}


function get_products_page($page_number)
{
    $ch = curl_init( "https://windshiptrading.com/browse-everything/?limit=" . LIMIT . "&sort=newest&page=" . $page_number );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, TRUE );

    $html = curl_exec( $ch );

    curl_close( $ch );

    return $html;
}

function get_products()
{
    $products_ids = [];

    for ( $i = 0, $j = 1; $i < PRODUCTS_TOTAL; $i += LIMIT, $j++ ) {
        $html = get_products_page( $j );
        $matches = [];
        preg_match_all( '/(?<=\/products\/)\d+(?=\/)/', $html, $matches );
        $ids = array_unique( $matches[0] );
        $ids = array_values( $ids );
        $products_ids = array_merge( $products_ids, $ids );
    }

    $products = [];
    for ( $i = 0; $i < PRODUCTS_TOTAL; $i++ ) {
        $product = fetch_product_data( $products_ids[$i] );
        array_push( $products, $product );
    }

    return $products;
}

$start = microtime( true );

file_put_contents( 'products.json', json_encode( get_products(), JSON_PRETTY_PRINT ) );

$finish = microtime( true );

$delta = $finish - $start;

echo 'Execution time: ' . (int)$delta . ' sec.';
